package com.quantumhu.mqtter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.PowerManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private boolean sendData = true;

    // constants
    private int LOOP_TIME = 15; // seconds
    private int GPS_REFRESH_TIME = 12; // seconds
    private int GPS_REFRESH_DISTANCE = 0; // metres

    // mqtt constants
    private String BROKER = "ssl://<IP-addr here>:8883";
    private String TOPIC = "loc";
    private String MQTT_USERNAME = "android-app";
    private String MQTT_PASSWORD = "app-android";

    private MqttAndroidClient CLIENT;
    private MqttConnectOptions MQTT_OPTIONS;
    private final MemoryPersistence persistence = new MemoryPersistence();

    public enum MQTT_STATUSES {
        ;
        private static final String GREEN = "#27ae60";
        private static final String RED = "#c0392b";
        private static final String INITIAL = "#ffffff";
    }

    // loop timer
    private Handler loopTimer = new Handler();
    private Runnable runnable;

    // saved gps coordinates
    private String lastLat;
    private String lastLong;

    private final LocationListener locListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location loc) {
            setLastLat(loc.getLatitude());
            setLastLong(loc.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onProviderDisabled(String provider) {}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // event listener for floating button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();

                // force gps send
                loop();
            }
        });

        // event listener for switch
        Switch gps_toggle = findViewById(R.id.gps_switch);
        gps_toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Switch sw = findViewById(view.getId());
                boolean checked = sw.isChecked();

                //alert(Boolean.toString(checked));

                if (checked) {
                    startLoop(LOOP_TIME);
                } else {
                    stopLoop();
                }
            }
        });

        alert(getString(R.string.gps_limitation_warning));

        // show initial status of mqtt
        setStatus(MQTT_STATUSES.INITIAL);

        // disable output box typing
        TextView output = findViewById(R.id.outputTxt);
        output.setKeyListener(null);

        // set up location
        LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    GPS_REFRESH_TIME * 1000, GPS_REFRESH_DISTANCE, locListener);
        } catch (SecurityException e) {
            setErrorText("Check location permissions! Error accessing GPS.");
        }
        // connect to mqtt
        MqttSetup(this.getApplicationContext());
        MqttConnect();

        CLIENT.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                setErrorText("MQTT connection lost");
                setStatus(MQTT_STATUSES.RED);
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
            }
        });

        // start GPS sending function
        startLoop(LOOP_TIME);

    }

    public void startLoop(int loopTime) {

        sendData = true;

        final int delay = loopTime;

        // run once at beginning
        loop();

        loopTimer.postDelayed(runnable = new Runnable() {
            public void run() {

                loop();

                loopTimer.postDelayed(runnable, delay * 1000);

            }
        }, delay * 1000);

    }

    public void stopLoop() {

        sendData = false;

        loopTimer.removeCallbacks(runnable);
    }

    public void loop() {

        if (sendData) {
            // do loop
            sendGPS();
        }

    }

    public void sendGPS() {

        if (lastLat == null || lastLong == null) {
            // if gps hasn't received new data
            return;
        }

        Date now = new Date();

        SimpleDateFormat formatter = new SimpleDateFormat("MMMM d, yyyy h:mm:ss a");

        String msg = formatter.format(now) + " >> " + lastLat + ", " + lastLong;

        publish(TOPIC, msg);
        outputAddLine(msg);

    }

    public void setLastLat(double lat) {
        lastLat = Double.toString(lat);
    }

    public void setLastLong(double longi) {
        lastLong = Double.toString(longi);
    }

    public void outputAddLine(String line) {

        TextView output = findViewById(R.id.outputTxt);
        output.setText(line + "\n\n" + output.getText());

    }

    public void alert(String txt) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setMessage(txt);
        builder1.setCancelable(true);

        builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alert1 = builder1.create();
        alert1.show();

    }

    public void setStatus(String colourStr) {

        ImageView icon = findViewById(R.id.mqtt_status);

        int colour = Color.parseColor(colourStr);

        icon.setColorFilter(colour);

    }

    public void setErrorText(String txt) {

        Handler errorWait = new Handler();

        final TextView error_msg = findViewById(R.id.error_msgs);
        final String msg = txt;

        error_msg.setText("");

        errorWait.postDelayed(new Runnable() {
            @Override
            public void run() {
                error_msg.setText(msg);
            }
        }, 500);

    }

    public void MqttSetup(Context context) {

        String clientId = MqttClient.generateClientId();

        CLIENT = new MqttAndroidClient(context, BROKER, clientId, persistence);

        MQTT_OPTIONS = new MqttConnectOptions();
        MQTT_OPTIONS.setUserName(MQTT_USERNAME);
        MQTT_OPTIONS.setPassword(MQTT_PASSWORD.toCharArray());
        MQTT_OPTIONS.setCleanSession(true);

        // for ssl connection
        if (BROKER.contains("ssl")) {
            SocketFactory.SocketFactoryOptions socketFactoryOptions = new SocketFactory.SocketFactoryOptions();
            try {
                socketFactoryOptions.withCaInputStream(context.getResources().openRawResource(R.raw.ca));
                MQTT_OPTIONS.setSocketFactory(new SocketFactory(socketFactoryOptions));
                setErrorText("Client id: " + clientId);
            } catch (IOException | NoSuchAlgorithmException | KeyStoreException | CertificateException | KeyManagementException | UnrecoverableKeyException e) {
                setErrorText(e.getMessage());
            }
        }

    }

    public void MqttConnect() {
        try {

            final IMqttToken token = CLIENT.connect(MQTT_OPTIONS);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    outputAddLine("> mqtt: connected, subscribing to topic \"" + TOPIC + "\"...");
                    subscribe(TOPIC, (byte) 0);

                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    outputAddLine("> mqtt: NOT connected; " + asyncActionToken.toString());
                    setStatus(MQTT_STATUSES.RED);
                }
            });
        } catch (MqttException e) {
            setStatus(MQTT_STATUSES.RED);
            setErrorText(e.getMessage());
        }
    }

    public void subscribe(String topic, byte qos) {

        try {
            CLIENT.subscribe(topic, qos);
            outputAddLine("> mqtt: subscribed");
            setStatus(MQTT_STATUSES.GREEN);
        } catch (MqttException e) {
            setErrorText(e.getMessage());
        }

    }

    public void publish(String topic, String msg) {
        MQTT_OPTIONS.setWill(topic, msg.getBytes(), 0, false);

        try {
            CLIENT.publish(topic, new MqttMessage(msg.getBytes()));
        } catch (MqttException e) {
            setErrorText(e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
