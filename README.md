## MQTT Map

Using an Android app that sends GPS data to an MQTT server every 15 seconds, I can track my whereabouts within the city on an interactive map.

### Info

This was a project to learn:

* how publishing / subscribing worked with MQTT
* how to set up Mosquitto (an open source MQTT server)
* how to create a basic Android app

I had a lot of difficulties with Android Studio, so I have no guarantee that this project will build properly right away if you download and try it.

The setup has Mosquitto on a central server, with the regular SSL MQTT protocol (on port 8883), as well as the WebSocket protocol (on port 9001). The Android app connects to this MQTT server via port 8883 and subscribes to a topic to send data. The website connects to this MQTT server via port 9001 and subscribes to the same topic to receive data. The received data is displayed on an interactive map. The website uses a library called OpenLayers to create the map. 