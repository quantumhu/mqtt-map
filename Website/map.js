// mqtt client
var client;
var CHANNEL = "loc";
var runOnce = false;

MQTT_setup();

// initiate local storage
var coords = {
	list: [
		["Default point #1", [-76.4849, 44.2291]],
		["Default point #2", [-76.489552, 44.229857]]
	]
};

// pick up local storage data if it exists
// otherwise use the default above
if (localStorage.coords) {
	coords = JSON.parse(localStorage.getItem("coords"));
} else {
	localStorage.setItem("coords", JSON.stringify(coords));
}

// connect mqtt when a single tile has loaded
var source = new ol.source.OSM();
source.on("tileloadend", function() {
	if (!runOnce) {
		
		client.connect({
			onSuccess: MQTT_onConnect,
			userName: "nodejs",
			password: "nodejs"
		});
		
		runOnce = true;
	}
});

// ==================== MAP GENERATION ===========================
var map = new ol.Map({
   layers: [
        new ol.layer.Tile({
            source: source
        })
    ],

  target: 'map',
  view: new ol.View({
	// centered at kingston
	// eventually centered at average of the points zoomed out
    center: ol.proj.fromLonLat([-76.490914, 44.229551]),
    zoom: 15
  })
});

// styles for the map features
var lineStyle = new ol.style.Style({
	stroke: new ol.style.Stroke({
		color: "#3399cc",
		width: 3
	})
});
var dotStyle = new ol.style.Style({
	image: new ol.style.Circle({
		radius: 4,
		fill: new ol.style.Fill({
			color: "#cccccc",
		}),
		stroke: new ol.style.Stroke({
			color: "#9b59b6",
			width: 2
		})
	})
});
	
// layer for line
var lineLayer = new ol.layer.Vector({
	source: new ol.source.Vector({
		features: []
	})
});
map.addLayer(lineLayer);

// make a line using first point to first point, we then add later points ourselves
var line_coords = [
	ol.proj.fromLonLat(coords.list[0][1]), ol.proj.fromLonLat(coords.list[0][1])
]
var line = new ol.Feature({
	geometry: new ol.geom.LineString(line_coords)
});
line.setStyle(lineStyle);
lineLayer.getSource().addFeature(line);

// layer for dots
var dotLayer = new ol.layer.Vector({
	source: new ol.source.Vector({
		features: []
	})
});
map.addLayer(dotLayer);

// generate the map and generate all points defined in coords
generateInitialPointsAndLine();

// function for adding points
function generateInitialPointsAndLine() {
	
	var dot;
	var coordinates;
	
	for (var i = 0; i < coords.list.length; i++) {
		
		coordinates = ol.proj.fromLonLat(coords.list[i][1]);
		
		dot = generateDot(coords.list[i][0], coordinates);
		dotLayer.getSource().addFeature(dot);
		
		if (i != 0) {
			// since we already added the first point twice, we only add the later ones
			line.getGeometry().appendCoordinate(coordinates);
		}
	}
	
}

// helper function to generate a dot object, we use this later
function generateDot(timestamp, coords) {
	
	var dot = new ol.Feature({
		geometry: new ol.geom.Point(coords)
	});
		
	dot.setProperties({
		"timestamp": timestamp
	});
	dot.setStyle(dotStyle);
	
	return dot;
	
}

// =================== CODE FOR THE POPUP ==========================

// popup code
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');

var overlay = new ol.Overlay({
    element: container,
    autoPan: true,
    autoPanAnimation: {
        duration: 250
    }
});
map.addOverlay(overlay);

// make the dotLayer selectable
var selectFeature = new ol.interaction.Select({
	layers: [dotLayer]
});

// show the timestamp of each dot on click
selectFeature.on('select', function(e) {
	var features = e.target.getFeatures().getArray()
	
	if (features.length != 0) {
		var coordinate = e.mapBrowserEvent.coordinate;

		content.innerHTML = '<b>Timestamp</b><br />' + features[0].getProperties().timestamp;
		overlay.setPosition(coordinate);
	}

});
map.addInteraction(selectFeature);

closer.onclick = function() {
	overlay.setPosition(undefined);
	closer.blur();
	return false;
};
map.on('singleclick', function(e) {
    overlay.setPosition(undefined);
    closer.blur();
});



// ==================== MQTT FUNCTIONS ==========================
function MQTT_setup() {
	// create a client instance
	client = new Paho.MQTT.Client("<ip addr here>", 9001, "android_viewer_map");
	
	// set callback handlers
	client.onConnectionLost = MQTT_onConnectionLost;
	client.onMessageArrived = MQTT_onMessageArrived;
}

function MQTT_onConnect() {
  // Once a connection has been made, make a subscription and send ok message
  console.log("> Connected");
  client.subscribe(CHANNEL);
  message = new Paho.MQTT.Message("> Web mqtt map viewer connected.");
  message.destinationName = CHANNEL;
  client.send(message);
}

function MQTT_onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("onConnectionLost: " + responseObject.errorMessage);
  }
}

function MQTT_onMessageArrived(message) {
	
	console.log(message.payloadString);
	
	if (message.payloadString.charAt(0) != ">") {
	
		var array = parseMessage(message.payloadString);
		console.log(array);
		
		// check if coords are not the same as previous time
		if (array[1] != coords.list[coords.list.length - 1][1]) {
			
			// save the coordinates
			saveCoords(array);
			
			// display on map
			var coordinates = ol.proj.fromLonLat(array[1]);
		
			var dot = generateDot(array[0], coordinates);
			dotLayer.getSource().addFeature(dot);
			
			line.getGeometry().appendCoordinate(coordinates);
			
			
		} else {
			return;
		}
	}

}

function parseMessage(msg) {
	
	// is gps coordinate message so parse
	var timestamp = msg.split(" >> ");

	var raw_coords = timestamp[1];
	timestamp = timestamp[0];
		
	var coords = raw_coords.split(", ");

	var arr = [timestamp, [parseFloat(coords[1]), parseFloat(coords[0])]];
	return arr;

}

function hasNumber(str) {
	return /\d/.test(str);
}

function saveCoords(array) {
	coords.list.push(array);
	localStorage.setItem("coords", JSON.stringify(coords));
}